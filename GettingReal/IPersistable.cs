namespace GettingReal;

public interface ISearchAble<T>
{
    List<T> Search(string query);
}
public interface IPersistable
{
    void Save(string? filePath = null);
    void Load(string? filePath = null);
}