using System.Text.Json;
using GettingReal.Models;

namespace GettingReal.Repositorys;

public interface IHoldRepository : ISearchAble<Hold>
{
    void AddHoldToList(Hold hold);
    Hold GetMedHoldId(Guid id);
}

public class HoldRepository : IHoldRepository, IPersistable
{
    private readonly List<Hold> _hold;

    public HoldRepository()
    {
        _hold = new List<Hold>();
    }

    public void AddHoldToList(Hold hold)
    {
        _hold.Add(hold);
    }

    public Hold? GetMedHoldId(Guid id)
    {
        return _hold.SingleOrDefault(hold => hold.HoldId == id);
    }

    public List<Hold> Search(string query)
    {
        return _hold
            .Where(hold => hold.Lokation.Område.Contains(query))
            .Where(hold => hold.Lokation.BedNr.Contains(query))
            .Where(hold => hold.Lokation.Comment.Contains(query))
            .Where(hold => hold.BlomsterSort.Navn.Contains(query))
            .Where(hold => hold.BlomsterSort.Id.ToString().Contains(query))
            .ToList();
    }
    
    public void Save(string? filePath = null)
    {
        var json = JsonSerializer.Serialize(_hold);
        File.WriteAllText(filePath?? "db.json", json);
    }

    public void Load(string? filePath = null)
    {
        var json = JsonSerializer.Serialize(_hold);
        File.WriteAllText(filePath??"db.json", json);
    }
}