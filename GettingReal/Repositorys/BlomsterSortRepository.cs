using GettingReal.Models;

namespace GettingReal.Repositorys;

public interface IBlomstersortRepository
{
    BlomsterSortModel AsparagusGijnlimModel(BlomsterSortModel asparagusGijnlimModel);
}
public class BlomsterSortRepository : IBlomstersortRepository
{
    public BlomsterSortModel AsparagusGijnlimModel(BlomsterSortModel asparagusGijnlimModel)
    {
        asparagusGijnlimModel = new BlomsterSortModel
        {
            VareNummer = int.Parse("091124").ToString(),
            VareNavn = "ASPARAGUS GIJNLIM",
            Container = 112,
            PlanteStatusBlomst = "Blomst",
        };
        return asparagusGijnlimModel;
    }
}