﻿namespace GettingReal.Models;
public class Lokation
{
    public string Område;
    public string BedNr;
    public string Comment;

    public Lokation(string område)
    {
        Område = område;
    }
    public Lokation(string område, string bedNr) : this(område)
    {
        BedNr = bedNr;
    }
    public Lokation(string område, string bedNr, string comment) : this(område, bedNr)
    {
        Comment = comment;
    }

    public static Lokation From(BlomsterSortModel model)
    {
        if (model.OmrådeLokation == null) throw new Exception("Mangler Område");
        if (model.BedLokation == null) throw new Exception("Mangler BedNr");

        return new Lokation(model.OmrådeLokation, model.BedLokation, model.Lokation3 ?? string.Empty);
    }
}