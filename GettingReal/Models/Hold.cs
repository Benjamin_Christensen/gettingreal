﻿namespace GettingReal.Models;
public class Hold
{
    public Guid HoldId { get; set; } = Guid.NewGuid();
    public BlomsterSort BlomsterSort { get; set; }
    public Lokation Lokation { get; set; }
    public Størrelse Størrelse { get; set; }
    public PlanteStatus PlanteStatus { get; set; }
    public LagerStatus LagerStatus { get; set; }
    public int Prio { get; set; }
    public int Antal { get; set; }
    public int BKval { get; set; }
    public int AKval { get; set; }

    public Hold(BlomsterSort blomsterSort, Lokation lokation, Størrelse størrelse)
    {
        BlomsterSort = blomsterSort;
        Lokation = lokation;
        Størrelse = størrelse;
    }
    public Hold(BlomsterSort blomsterSort, Lokation lokation, Størrelse størrelse, PlanteStatus planteStatus, int prio, int antal, int bKval, int aKval) : this(blomsterSort, lokation, størrelse)
    {
        
        PlanteStatus = planteStatus;
        Prio = prio;
        Antal = antal;
        BKval = bKval;
        AKval = aKval;

    }

    public static Hold From(BlomsterSortModel model)
    {
        return new Hold(BlomsterSort.From(model), Lokation.From(model), Størrelse.From(model));
    }

    public override string ToString()
    {
        return base.ToString();
    }
}
