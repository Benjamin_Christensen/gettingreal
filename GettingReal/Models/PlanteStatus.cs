namespace GettingReal.Models;

public enum PlanteStatus
{
    Blomst,
    Knop,
    Blad,
    Bær

}