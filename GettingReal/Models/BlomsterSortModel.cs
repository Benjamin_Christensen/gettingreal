using System.Text.Json.Serialization;

namespace GettingReal.Models;

//dette er et DTO(Data Transfer Object) object så dette object er til at parse data
//
public class BlomsterSortModel
{
    public string? VareNummer { get; set; }
    public string? VareNavn { get; set;}
    public string? Størrelse { get; set; }
    public bool OnWebsite { get; set; }
    public int? Prioritet { get; set; }
    public int? Disponible { get; set; }
    public int? Reserveret { get; set; }
    public int? WebAntal { get; set; }
    public int? BKval { get; set; }
    public int? SalgsLager { get; set; }
    public int? AntalPlanterTotal { get; set; }
    public int? Container { get; set; }
    public string? PlanteStatusBlomst { get; set; }
    public string? PlanteStatusKnop { get; set; }
    public string? PlanteStatusBlad { get; set; }
    public string? PlanteStatusBær { get; set; }
    public string? OmrådeLokation { get; set; }
    public string? BedLokation { get; set; }
    public string? Lokation3 { get; set; }
    public string? Bemærkninger { get; set; }
    public string? Sludretekst { get; set; }
    
}