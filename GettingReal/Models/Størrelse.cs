namespace GettingReal.Models;

public class Størrelse
{
    public string Size;
    public Størrelse(string size)
    {
        Size = size;
    }

    public static Størrelse From(BlomsterSortModel model)
    {
        if (model.Størrelse == null) throw new Exception("Mangler Størrelse");

        return new Størrelse(model.Størrelse);

    }

}