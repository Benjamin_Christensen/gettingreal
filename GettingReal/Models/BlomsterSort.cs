namespace GettingReal.Models;

public class BlomsterSort
{
    public string Id;
    public string Navn;
    public string Nyhed;
    public int AntalPrCC;

    public BlomsterSort(string id)
    {
        Id = id;
    }
    public BlomsterSort(string id, string navn) : this(id)
    {
        Navn = navn;
    }
    
    public static BlomsterSort From(BlomsterSortModel model)
    {
        if (model.VareNummer == null) throw new Exception("Mangler VareNummer");
        if (model.VareNavn == null) throw new Exception("Mangler Varenavn ");
        return new BlomsterSort(model.VareNummer, model.VareNavn)
        {
            AntalPrCC = model.Container??0 // hvis container er null får den et 0
        };
    }
}