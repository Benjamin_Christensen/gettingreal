namespace GettingReal.Models;

public class LagerStatus
{
    public bool OnWeb { get; set; }
    public int WebStk { get; set; }
    public int Disponible { get; set; }
    public int Reseveret { get; set; }
    public bool SalgsKlar { get; set; }

}