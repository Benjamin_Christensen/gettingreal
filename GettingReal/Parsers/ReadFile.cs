namespace GettingReal.Parsers;

// Laver kun denne class for at kunne teste parser.
public interface IReadFile
{
    string ReadAllText(string filepath);
    string[] ReadAllLines(string filepath);
}

public class ReadFile : IReadFile
{
    public string ReadAllText(string filepath) => File.ReadAllText(filepath);

    public string[] ReadAllLines(string filepath) => File.ReadAllLines(filepath);
}