using System.Text.Json;
using GettingReal.Models;

namespace GettingReal.Parsers;

public interface IParser<T>
{

    List<T> Parse(string filePath);
}

public class JsonBlomsterSortModelParser : IParser<BlomsterSortModel>
{

    public List<BlomsterSortModel> Parse(string filePath)
    {
        var readJsonFile = File.ReadAllText(filePath);
        var models = JsonSerializer.Deserialize<List<BlomsterSortModel>>(readJsonFile);
        return models;
    }
}