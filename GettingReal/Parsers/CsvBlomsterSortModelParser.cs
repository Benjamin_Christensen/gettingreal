using GettingReal.Models;

namespace GettingReal.Parsers;

public class CsvBlomsterSortModelParser : IParser<BlomsterSortModel> 
{
    private readonly IReadFile _readFile;

    public CsvBlomsterSortModelParser(IReadFile readFile)
    {
        _readFile = readFile;
    }
    private static List<BlomsterSortModel> Parse(string[] data)
    {
        var blomsterSortList = new List<BlomsterSortModel>();
        for (var i = 0; i < data.Count(); i++)
        {
            if (i == 0) continue;

            var dataSplit = data[i].Split(";");
            var blomsterSort = new BlomsterSortModel
            {
                VareNummer =!string.IsNullOrWhiteSpace(dataSplit[1])? dataSplit[1]: null,
                VareNavn = !string.IsNullOrWhiteSpace(dataSplit[2])? dataSplit[2]: null,
                Størrelse = !string.IsNullOrWhiteSpace(dataSplit[3])? dataSplit[3]: null,
                OnWebsite = !string.IsNullOrEmpty(dataSplit[4]),
                Prioritet = !string.IsNullOrWhiteSpace(dataSplit[5]) ? int.Parse(dataSplit[5]) : null,
                Disponible = !string.IsNullOrWhiteSpace(dataSplit[6]) ? int.Parse(dataSplit[6]) : null,
                Reserveret = !string.IsNullOrWhiteSpace(dataSplit[7]) ? int.Parse(dataSplit[7]) : null,
                WebAntal = !string.IsNullOrWhiteSpace(dataSplit[8]) ? int.Parse(dataSplit[8]) : null,
                BKval = !string.IsNullOrWhiteSpace(dataSplit[9]) ? int.Parse(dataSplit[9]) : null,
                SalgsLager = !string.IsNullOrWhiteSpace(dataSplit[10]) ? int.Parse(dataSplit[10]) : null,
                AntalPlanterTotal = !string.IsNullOrWhiteSpace(dataSplit[11]) ? int.Parse(dataSplit[11]) : null,
                Container = !string.IsNullOrWhiteSpace(dataSplit[12]) ? int.Parse(dataSplit[12]) : null,
                PlanteStatusBlomst = !string.IsNullOrWhiteSpace(dataSplit[13])? dataSplit[13] : null,
                PlanteStatusKnop = !string.IsNullOrWhiteSpace(dataSplit[14])? dataSplit[14] : null,
                PlanteStatusBlad = !string.IsNullOrWhiteSpace(dataSplit[15])? dataSplit[15] : null,
                PlanteStatusBær = !string.IsNullOrWhiteSpace(dataSplit[16])? dataSplit[16] : null,
                OmrådeLokation = !string.IsNullOrWhiteSpace(dataSplit[17])? dataSplit[17] : null,
                BedLokation = !string.IsNullOrWhiteSpace(dataSplit[18])?dataSplit[18] : null,
                Lokation3 = !string.IsNullOrWhiteSpace(dataSplit[19])? dataSplit[19] : null,
                Bemærkninger = !string.IsNullOrWhiteSpace(dataSplit[20])? dataSplit[20] : null,
                Sludretekst = !string.IsNullOrWhiteSpace(dataSplit[21])? dataSplit[21] : null,
                
            };
            blomsterSortList.Add(blomsterSort);
        }

        return blomsterSortList;
    }

    public List<BlomsterSortModel> Parse(string filePath)
    {
        var readAllLines = _readFile.ReadAllLines(filePath);
        return Parse(readAllLines);
    }
}