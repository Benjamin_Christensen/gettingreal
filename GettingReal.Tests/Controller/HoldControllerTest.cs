using GettingReal.Controller;
using GettingReal.Models;
using GettingReal.Repositorys;

namespace GettingReal.Tests.Controller;

public class HoldControllerTest
{
    private readonly HoldController _sut;
    private readonly IHoldRepository _holdRepository;

    public HoldControllerTest()
    {
        _holdRepository = new HoldRepository();
        _sut = new HoldController(_holdRepository);
    }

    [Fact]
    public void GivenHoldSHouldCreatHold()
    {
        var v1 = new Hold(new BlomsterSort("1"), new Lokation("B2"), new Størrelse("110"));
        var v2 = new Hold(new BlomsterSort("2"), new Lokation("B3"), new Størrelse("120")
            , PlanteStatus.Blomst, 20, 10, 5, 2);
        // _sut.OpretHold(v1);
        // _sut.OpretHold(v2);
        var actuel = _holdRepository.GetMedHoldId(v1.HoldId);
        Assert.NotNull(actuel);
    }
}