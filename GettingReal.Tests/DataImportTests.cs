using System.Text.Json;
using GettingReal.Models;

namespace GettingReal.Tests;

public class Test2
{
    [Fact]
    public void GivenCSVLineShouldSplitOnSemiColon()
    {
        var csvData = ";091124;ASPARAGUS GIJNLIM;BK 6;ja;10;422;1465;361;;1812;1812;112;.;.;.;.;N 1;48-65;;;14 bk. pr. lag.";
        
        var dataSplit = csvData.Split(";");
        var blomsterSort = new BlomsterSortModel()
        {
            VareNummer = dataSplit[1],
            VareNavn = dataSplit[2],
            Container = int.Parse(dataSplit[12])

        };
        
        Assert.Equal(22,dataSplit.Length );
        Assert.Equal(112, blomsterSort.Container);
    }
    
    [Fact]
    public void GivenCSVLineShouldMapToBlomsterSort()
    {
        var csvData = ";091124;ASPARAGUS GIJNLIM;BK 6;ja;10;422;1465;361;;1812;1812;112;.;.;.;.;N 1;48-65;;;14 bk. pr. lag.";
        
        var dataSplit = csvData.Split(";");
        var blomsterSort = new BlomsterSortModel()
        {
            VareNummer = dataSplit[1],
            VareNavn = dataSplit[2],
            Container = int.Parse(dataSplit[12])

        };
        
        Assert.Equal(112, blomsterSort.Container);
    }

    [Fact]
    public void GivenFilePathSHouldReturnArrayWithLines()
    {
        var importer = File.ReadAllLines("Ugelisten.csv");
        Assert.True(importer.Any());
    }

    [Fact]
    public void GivenCSVShouldAddGivenParametersToList()
    {
        var blomstersortList = new List<BlomsterSortModel>();
        var import = File.ReadAllLines("Ugelisten.csv");
        for (int i = 0; i < import.Length; i++)
        {
            if (i==0)
            {
                continue;
            }

            var dataSplit = import[i].Split(";");
            var blomsterSort = new BlomsterSortModel()
            {
                VareNummer =!string.IsNullOrWhiteSpace(dataSplit[1])? dataSplit[1]: null,
                VareNavn = !string.IsNullOrWhiteSpace(dataSplit[2])? dataSplit[2]: null,
                Størrelse = !string.IsNullOrWhiteSpace(dataSplit[3])? dataSplit[3]: null,
                OnWebsite = !string.IsNullOrEmpty(dataSplit[4]),
                Prioritet = !string.IsNullOrWhiteSpace(dataSplit[5]) ? int.Parse(dataSplit[5]) : null,
                Disponible = !string.IsNullOrWhiteSpace(dataSplit[6]) ? int.Parse(dataSplit[6]) : null,
                Reserveret = !string.IsNullOrWhiteSpace(dataSplit[7]) ? int.Parse(dataSplit[7]) : null,
                WebAntal = !string.IsNullOrWhiteSpace(dataSplit[8]) ? int.Parse(dataSplit[8]) : null,
                BKval = !string.IsNullOrWhiteSpace(dataSplit[9]) ? int.Parse(dataSplit[9]) : null,
                SalgsLager = !string.IsNullOrWhiteSpace(dataSplit[10]) ? int.Parse(dataSplit[10]) : null,
                AntalPlanterTotal = !string.IsNullOrWhiteSpace(dataSplit[11]) ? int.Parse(dataSplit[11]) : null,
                Container = !string.IsNullOrWhiteSpace(dataSplit[12]) ? int.Parse(dataSplit[12]) : null,
                PlanteStatusBlomst = !string.IsNullOrWhiteSpace(dataSplit[13])? dataSplit[13] : null,
                PlanteStatusKnop = !string.IsNullOrWhiteSpace(dataSplit[14])? dataSplit[14] : null,
                PlanteStatusBlad = !string.IsNullOrWhiteSpace(dataSplit[15])? dataSplit[15] : null,
                PlanteStatusBær = !string.IsNullOrWhiteSpace(dataSplit[16])? dataSplit[16] : null,
                OmrådeLokation = !string.IsNullOrWhiteSpace(dataSplit[17])? dataSplit[17] : null,
                BedLokation = !string.IsNullOrWhiteSpace(dataSplit[18])?dataSplit[18] : null,
                Lokation3 = !string.IsNullOrWhiteSpace(dataSplit[19])? dataSplit[19] : null,
                Bemærkninger = !string.IsNullOrWhiteSpace(dataSplit[20])? dataSplit[20] : null,
                Sludretekst = !string.IsNullOrWhiteSpace(dataSplit[21])? dataSplit[21] : null,
                
            };
            blomstersortList.Add(blomsterSort);
        }
        
        Assert.Equal(import.Length-1, blomstersortList.Count);
        
    }

    [Fact]
    public void GivenBlomsortListParseDataFromList()
    {
        
    }
}