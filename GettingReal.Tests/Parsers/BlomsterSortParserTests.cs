using GettingReal.Models;
using GettingReal.Parsers;

namespace GettingReal.Tests.Parsers;

public class ParserTests
{
    private CsvBlomsterSortModelParser _sut;
    private TestReadFile _readFile;

    public ParserTests()
    {
        _readFile = new TestReadFile();
        _sut = new CsvBlomsterSortModelParser(_readFile);
    }
    
    [Fact]
    public void GivenCsvLineShouldMapToBlomsterSort()
    {
        var csvData = ";091123;ASPARAGUS GIJNLIM;BK 6;ja;10;422;1465;361;;1812;1812;112;.;.;.;.;N 1;48-65;;;14 bk. pr. lag.";
        string[] listOfStrings = 
        {
            Header(),
            csvData
        };
        _readFile.AddLines(listOfStrings);
        var blomsterSortModels = _sut.Parse("");
        var firstBlomsterSortModel = blomsterSortModels.First();
        Assert.Equal("091123", firstBlomsterSortModel.VareNummer);
    }

    private string Header()
    {
        return
            ";Vare nr.;Varenavn;Str;Web;Prio;Disp;Resv;Web stk;B-kval;Salgslager;Antal planter i alt;CC;Blomst;Knop;Blad;B�r;Lokation1;Lokation2;Lokation3;Bem�rkninger;Sludretekst";
    }

    [Fact]

    public void GivenFilePathSHouldReturnArrayWithLines()
    {
        var blomsterSortModels = _sut.Parse("Ugelisten.csv");
        var firstBlomsterSortModel = blomsterSortModels.First();
        var secondBlomsterSortModel = blomsterSortModels[0];
        Assert.Equal("091124", firstBlomsterSortModel.VareNummer);
        Assert.Equal("ASPARAGUS GIJNLIM", secondBlomsterSortModel.VareNavn);
    }


}