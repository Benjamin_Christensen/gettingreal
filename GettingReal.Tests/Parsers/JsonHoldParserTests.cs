using GettingReal.Models;
using GettingReal.Parsers;

namespace GettingReal.Tests.Parsers;

public class JsonHoldParserTests
{
    private readonly JsonBlomsterSortModelParser _sut;

    public JsonHoldParserTests()
    {
        _sut = new JsonBlomsterSortModelParser();
    }

    [Fact]
    public void GivenBlomsterSortModelCanMapToHold()
    {
        var blomsterSortModels =_sut.Parse("UgelisteJson.json");
        var firstBlomstersortmodel = blomsterSortModels.First();
        var actuel = Hold.From(firstBlomstersortmodel);
        Assert.NotNull(actuel);
    }
}