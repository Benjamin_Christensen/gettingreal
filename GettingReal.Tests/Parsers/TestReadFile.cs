using GettingReal.Parsers;

namespace GettingReal.Tests.Parsers;

public class TestReadFile : IReadFile
{
    private List<string> _listOfStrings = new List<string>();

    public void AddLine(string line)
    {
        _listOfStrings.Add(line);
    }

    public void AddLines(IEnumerable<string> lines)
    {
        _listOfStrings.AddRange(lines);
    }

    public string ReadAllText(string filepath)
    {
        var stringJoiner = string.Join('\n', _listOfStrings);
        return stringJoiner;
    }

    public string[] ReadAllLines(string filepath)
    {
        return _listOfStrings.ToArray();
    }
}